import base64

from fastapi import status

from athome.photos import schemas
from athome.photos.config import Config, set_config
from athome.photos.utils import resource

# noinspection PyUnresolvedReferences
from utils import TestClient, generate_ersatz_token


config = Config(
    db={
        'path': '../../testdb',
        'sqlite_db_filename': '.db',
    },
    auth={
        'hostname': '127.0.0.1',
        'port': 5000,
        'key': {
            'location': resource('auth_server.pub', test=True),
        }
    },
    server={
        'hostname': 'testserver',
        'port': None,
    },
)
set_config(config)


def test_image_access_forbidden():
    """
    Test that images stored under one user cannot be accessed by another
    """

    # Get raw image
    with open(resource('rick_roll.jpg', test=True), 'rb') as image_resource_file:
        image_bytes = image_resource_file.read()
    image_base64 = base64.b64encode(image_bytes).decode('ascii')
    test_image = schemas.Image(
        id='e7740a1d-0dcd-4199-b714-26f597147d8d',
        path='me.jpg',
        image=image_base64
    )
    test_user = schemas.User(username='rick')
    test_user_imposter = schemas.User(username='bick')

    test_id_token = generate_ersatz_token(test_user.username, {})
    test_id_token_imposter = generate_ersatz_token(test_user_imposter.username, {})

    with TestClient(config) as client:

        # Post image
        response = client.post(
            f'/image/{test_image.id}',
            data=test_image.json(),
            headers={'Authorization': f'Bearer {test_id_token}'},
        )
        assert response.status_code == status.HTTP_200_OK

        # Get image
        response = client.get(
            f'/image/{test_image.id}',
            headers={'Authorization': f'Bearer {test_id_token_imposter}'},
        )
        assert response.status_code == status.HTTP_403_FORBIDDEN
