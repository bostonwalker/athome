import base64

from fastapi import status

from athome.photos.config import Config
from athome.photos.utils import resource
from athome.photos import schemas

# noinspection PyUnresolvedReferences
from utils import TestClient, generate_ersatz_token


config = Config(
    db={
        'path': '../../testdb',
        'sqlite_db_filename': '.db',
    },
    auth={
        'hostname': '127.0.0.1',
        'port': 5000,
        'key': {
            'location': '../../resource/test/auth_server.pub',
        }
    },
    server={
        'hostname': 'testserver',
        'port': None,
    },
)


def test_retrieve_image():

    # Get raw image
    with open(resource('rick_roll.jpg', test=True), 'rb') as image_resource_file:
        image_bytes = image_resource_file.read()
    image_base64 = base64.b64encode(image_bytes).decode('ascii')
    test_image = schemas.Image(
        id='e7740a1d-0dcd-4199-b714-26f597147d8d',
        path='me.jpg',
        image=image_base64
    )
    test_user = schemas.User(username='rick')

    test_id_token = generate_ersatz_token(test_user.username, {})

    with TestClient(config) as client:

        # Post image
        response = client.post(
            f'/image/{test_image.id}',
            data=test_image.json(),
            headers={'Authorization': f'Bearer {test_id_token}'},
        )
        assert response.status_code == status.HTTP_200_OK

        # Get image
        response = client.get(
            f'/image/{test_image.id}',
            headers={'Authorization': f'Bearer {test_id_token}'},
        )
        assert response.status_code == status.HTTP_200_OK

        # Verify data
        assert response.headers.get('Content-Type') == 'image/jpeg'
        assert response.content == image_bytes
