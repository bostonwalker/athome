from urllib.parse import urlparse, parse_qs

from fastapi import status

# noinspection PyUnresolvedReferences
from utils import TestClient

from athome.photos.config import Config


config = Config(
    db={
        'path': '../../testdb',
        'sqlite_db_filename': '.db',
    },
    auth={
        'hostname': '127.0.0.1',
        'port': 5000,
        'key': {
            'location': '../../resource/test/auth_server.pub',
        }
    },
    server={
        'hostname': 'testserver',
        'port': 5001,
    },
)


# noinspection PyTypeChecker
def test_login_redirect():
    """
    Test that attempting to access a secured resource produces a redirect to a login page on the authorization server
    """

    test_image_id = 'e7740a1d-0dcd-4199-b714-26f597147d8d'

    with TestClient(config) as client:
        # Perform imports
        from athome.photos.auth import CLIENT_ID

        # Get image
        response = client.get(f'/image/{test_image_id}', allow_redirects=False)
        assert response.status_code == status.HTTP_303_SEE_OTHER
        assert response.is_redirect
        assert not response.is_permanent_redirect

        redirect_url = urlparse(response.headers['location'])
        redirect_params = parse_qs(redirect_url.query)

        assert redirect_url.scheme == 'http'
        assert redirect_url.hostname == config.auth.hostname
        assert redirect_url.port == config.auth.port
        assert redirect_url.path == '/auth'

        assert redirect_params['response_type'][0] == 'code'
        assert redirect_params['client_id'][0] == CLIENT_ID
        assert 'code_challenge' in redirect_params
        assert redirect_params['code_challenge_method'][0] == 'S256'
        assert 'state' in redirect_params

        token_redirect_url = urlparse(redirect_params['redirect_url'][0])

        assert token_redirect_url.scheme == 'http'
        assert token_redirect_url.hostname == config.server.hostname
        assert token_redirect_url.port == config.server.port
        assert token_redirect_url.path == '/auth/token'
