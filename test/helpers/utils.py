from requests import PreparedRequest
from typing import Optional

from datetime import timedelta, datetime

from cryptography.hazmat.primitives._serialization import Encoding, PrivateFormat, NoEncryption
from cryptography.hazmat.primitives.serialization import load_pem_private_key
from jose import jwt

from fastapi import Response, Request
from fastapi.testclient import TestClient as FastAPITestClient

from athome.photos.config import Config, set_config
from athome.photos.utils import resource


def pretty_format_request(request: Request):
    _cr_nl = '\r\n'
    return (f'-----------START-----------\n'
            f'{request.method} {request.url}\r\n'
            f'{_cr_nl.join(f"{k}: {v}" for k, v in request.headers.items())}\r\n\r\n'
            f'{request.body}')


class TestClient(FastAPITestClient):
    """
    Context manager class for performing tests

    Functionality:
    - Manages global config
    - Sets up and tears down DB
    - Prints requests nicely

    :config
    """
    def __init__(self, config: Optional[Config] = None, **kwargs):

        # Set global config before imports
        if config is not None:
            set_config(config)
        else:
            del config
            from athome.photos.config import config

        from athome.photos.main import app

        super().__init__(app, base_url=f'http://{config.server.hostname}', **kwargs)

    def __enter__(self):
        from athome.photos.db import init_db
        init_db()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        from athome.photos.db import delete_db
        delete_db()

    def send(self, request: PreparedRequest, **kwargs) -> Response:
        """
        Send a given PreparedRequest.

        :param request: Prepared request
        :return: Response
        """
        print(pretty_format_request(request))
        return super().send(request, **kwargs)


def generate_ersatz_token(username: str, data: dict, expires_delta: Optional[timedelta] = None,
                          keyfile: Optional[str] = None, password: Optional[str] = None) -> str:
    """
    Create spoofed JWT access token

    :param username: Authenticated user
    :param data: Data to be encoded in token
    :param expires_delta: Timedelta until expiry
    :param keyfile: Filepath of private key used to generate token
    :param password: Password of private key used to generate token
    :return: JWT access token
    """
    expires_delta = expires_delta or timedelta(days=7)
    keyfile = keyfile or resource('auth_server', test=True)
    password = password or '1234'

    # RSA private key for signing authorization codes and tokens
    with open(keyfile, 'r') as key_file:
        rsa_private_key = load_pem_private_key(
            key_file.read().encode(), password=password.encode()
        ).private_bytes(Encoding.PEM, PrivateFormat.PKCS8, NoEncryption())

    expire_time = datetime.utcnow() + expires_delta
    to_encode = data.copy() | {'sub': username, 'exp': expire_time}
    # noinspection PyTypeChecker
    encoded_jwt = jwt.encode(to_encode, rsa_private_key, algorithm='RS256')
    return encoded_jwt
