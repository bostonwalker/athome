from fastapi import APIRouter, Depends, HTTPException, status
from fastapi.responses import FileResponse

from sqlalchemy.orm import Session

from .. import schemas
from ..auth import get_current_user
from ..db import get_db
from ..image.api import get_image_by_id, create_image


router = APIRouter(
    prefix='/image',
    tags=['image'],
)


@router.get('/{image_id}')
def get_image(image_id: str, db: Session = Depends(get_db), user: schemas.User = Depends(get_current_user)
              ) -> FileResponse:
    """
    GET image by id

    :param image_id: Image UUID in database
    :param user: Authorized user
    :param db: DB session
    :return: Image object with base64 data populated
    """
    image = get_image_by_id(db, image_id)

    if image is None:
        raise HTTPException(
            status.HTTP_404_NOT_FOUND,
        )

    if image.username != user.username:
        raise HTTPException(
            status.HTTP_403_FORBIDDEN,
            detail='You don\'t have access to this resource',
        )

    return FileResponse(image.path_in_filesystem())


@router.post('/{image_id}')
def post_image(image_id: str, image: schemas.Image, user: schemas.User = Depends(get_current_user),
               db: Session = Depends(get_db)) -> schemas.Image:
    """
    POST image by id

    :param image_id: Image UUID in database
    :param user: Authorized user
    :param image: Image JSON
    :param db: DB session
    :return: Image object without base64 data
    """
    # TODO: handle internal errors and return appropriate response codes
    assert image_id == image.id
    return create_image(db, user, image)
