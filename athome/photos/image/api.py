from typing import Tuple, Callable, Optional

import base64
import os

from sqlalchemy.orm import Session

from ..config import config
from .. import models, schemas


def get_image_by_id(db: Session, image_id: str) -> Optional[models.Image]:
    """
    Get image from database/filesystem

    :param db: DB session
    :param image_id: Image UUID in database
    :return: Image object, if found
    """
    return db.query(models.Image).filter(models.Image.id == image_id).one_or_none()


def create_image(db: Session, user: schemas.User, image: schemas.Image) -> models.Image:
    """
    Create image in database/filesystem

    :param db: DB session
    :param user: User
    :param image: Image JSON
    :return: Image object without base64 data
    """

    if image.image is None:
        raise TypeError('Image must contain base64 data')

    # TODO: ensure path is well-formatted
    # TODO: ensure username is formatted correctly and path is within user directory only
    path_full = os.path.join(os.path.abspath(config.db.path), user.username, image.path)

    # TODO: ensure image data is not corrupted
    image_bytes = base64.b64decode(image.image.encode('ascii'))

    # TODO: prevent SQL injection attack
    doc = models.Image(id=image.id, username=user.username, path=image.path)

    # TODO: ensure image file / db entry does not already exist
    # Atomic write operation to DB / filesystem
    try:
        db.add(doc)
        db.commit()
        db.refresh(doc)
        os.makedirs(os.path.dirname(path_full), exist_ok=True)
        with open(path_full, 'wb') as image_file:
            image_file.write(image_bytes)
        doc.image = None
        return doc
    except Exception as e:
        # Clean up
        db.delete(doc)
        db.commit()
        try:
            os.remove(path_full)
        except OSError:
            pass
        raise e
