from typing import Optional

from pydantic import BaseModel


class Token(BaseModel):
    """
    Token used in authentication
    """
    access_token: str
    token_type: str
    expires_in: Optional[int]  # Seconds until expiry
    refresh_token: Optional[str]
    scope: Optional[str]


class User(BaseModel):
    """
    Pydantic schema for user information
    """
    username: str


class Image(BaseModel):
    """
    Pydantic schema for JSON images
    """
    # UUID of image
    id: str
    # Path to image file
    path: str
    # Image file encoded in base64
    image: Optional[str] = None
