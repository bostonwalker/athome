import os

from sqlalchemy import Column, String

from .config import config
from .db import BaseModel


class Image(BaseModel):
    """
    'images' table in the database
    """
    __tablename__ = 'images'

    id = Column(String(36), primary_key=True)
    username = Column(String(24), index=True)
    path = Column(String(4096), index=True, unique=True)

    def path_in_filesystem(self) -> str:
        """
        Get path in filesystem of image

        :return: Absolute path in filesystem
        """
        return os.path.abspath(os.path.join(config.db.path, self.username, self.path))
