import uvicorn
from fastapi import FastAPI, Request, Response

from athome.photos.config import config
from athome.photos.db import init_db
from athome.photos.auth import router as auth, HTTPUnauthorizedException, redirect_authorization
from athome.photos.image import router as image


app = FastAPI()

app.include_router(auth)
app.include_router(image)


@app.exception_handler(HTTPUnauthorizedException)
async def redirect_to_login(request: Request, exc: Exception) -> Response:
    print(f'redirect_to_login(): {exc}')
    return await redirect_authorization(request)


if __name__ == '__main__':
    init_db()
    # noinspection PyTypeChecker
    uvicorn.run(app, host=config.server.hostname, port=config.server.port)
