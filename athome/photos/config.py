import os
import yaml


class Config(object):

    class Server(object):

        def __init__(self, hostname: str, port: int):
            self.__hostname = hostname
            self.__port = port

        @property
        def hostname(self) -> str:
            return self.__hostname

        @property
        def port(self) -> int:
            return self.__port

    class Auth(object):

        class Key(object):

            def __init__(self, location: str):
                self.__location = location

            @property
            def location(self) -> str:
                return self.__location

        def __init__(self, hostname: str, port: int, key: dict):
            self.__hostname = hostname
            self.__port = port
            self.__key = Config.Auth.Key(**key)

        @property
        def hostname(self) -> str:
            return self.__hostname

        @property
        def port(self) -> int:
            return self.__port

        @property
        def key(self) -> Key:
            return self.__key

    class DB(object):

        def __init__(self, path: str, sqlite_db_filename: str):
            self.__path = path
            self.__sqlite_db_filename = sqlite_db_filename

        @property
        def path(self) -> str:
            return self.__path

        @property
        def sqlite_db_filename(self) -> str:
            return self.__sqlite_db_filename

    def __init__(self, server: dict, auth: dict, db: dict):
        self.__server = Config.Server(**server)
        self.__auth = Config.Auth(**auth)
        self.__db = Config.DB(**db)

    @property
    def server(self) -> Server:
        return self.__server

    @property
    def auth(self) -> Auth:
        return self.__auth

    @property
    def db(self) -> DB:
        return self.__db

    @staticmethod
    def from_yaml(yaml_str: str) -> 'Config':
        """
        Parse YAML config

        :param yaml_str: YAML config as string
        :return: Config object
        """
        yaml_dict = yaml.load(yaml_str, yaml.Loader)
        return Config(**yaml_dict)


_path = os.path.abspath(__file__)
_config_path = os.path.join(os.path.dirname(_path), '../photosathome.yml')
with open(_config_path, 'r') as config_file:
    config = Config.from_yaml(config_file.read())


# Monkey patch
def set_config(new_config: Config):
    global config
    config = new_config


def test_config():
    yaml_str = """
    db:
        path: '/a/b/'
        sqlite_db_filename: '.c'
    auth:
        hostname: 127.0.0.1
        port: 5000
        key:
            location: auth.pub
    server:
        hostname: localhost
        port: 5001
    """
    config = Config.from_yaml(yaml_str)

    assert config.db.path == '/a/b/'
    assert config.db.sqlite_db_filename == '.c'

    assert config.auth.hostname == '127.0.0.1'
    assert config.auth.port == 5000
    assert config.auth.key.location == 'auth.pub'

    assert config.server.hostname == 'localhost'
    assert config.server.port == 5001
