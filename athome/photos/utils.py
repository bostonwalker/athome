import os


def resource(path: str, test: bool = False) -> str:
    """
    Return resource path

    :param path: Relative path in resources directory
    :param test: If False, return path in resource/main. If True, return path in resource/test
    :return: Absolute path to resource file
    """
    return os.path.abspath(os.path.join(
        os.path.dirname(__file__),
        '../../resource',
        'test' if test else 'main',
        path,
    ))
