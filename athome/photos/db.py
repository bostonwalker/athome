from typing import Generator

import os
import shutil

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, declarative_base, Session

from .config import config


def path_in_db(path: str) -> str:
    """
    Convert a relative filepath into an absolute OS filesystem path
    :param path: Relative filepath in DB
    :return: Absolute filepath in OS filesystem
    """
    assert path != '/'  # Don't try this at home, kids
    return os.path.join(os.path.abspath(config.db.path), path)


def _sqlalchemy_database_url():
    """
    Build SQLite URI from config
    """
    sqlite_db_filepath = path_in_db(config.db.sqlite_db_filename)
    return f'sqlite:////{sqlite_db_filepath}'


# Connect to SQLite allowing multiple threads to take advantage of FastAPI concurrency
engine = create_engine(_sqlalchemy_database_url(), connect_args={'check_same_thread': False})

# Session
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

# Base class for ORM models
BaseModel = declarative_base()


def init_db() -> None:
    """
    Initialize SQLite db
    """
    # Create folder for db file to prevent error
    os.makedirs(os.path.abspath(config.db.path), exist_ok=True)

    # Initialize all tables
    BaseModel.metadata.create_all(bind=engine)


def delete_db() -> None:
    """
    Delete db and all files within
    """
    shutil.rmtree(path_in_db(''))


def get_db() -> Generator[Session, None, None]:
    """
    Get db session
    """
    # Create SessionLocal
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()
