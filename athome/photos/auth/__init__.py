from typing import Dict, Optional, Any, Union

import logging
from urllib.parse import urlencode
import requests

from fastapi.openapi.models import OAuthFlows as OAuthFlowsModel
from fastapi.security.utils import get_authorization_scheme_param
from fastapi import APIRouter, Request, Response, HTTPException, status, Depends
from fastapi.responses import RedirectResponse
from fastapi.security import OAuth2

from .. import schemas
from ..config import config
from .crypto import base64_random, generate_code_challenge, decode_access_token


log = logging.getLogger('uvicorn')


# TODO: refresh tokens, scopes
class OAuth2Scheme(OAuth2):
    """
    Custom authentication scheme
    """
    def __init__(self, authorization_url: str, token_url: str, refresh_url: Optional[str] = None,
                 scheme_name: Optional[str] = None, scopes: Optional[Dict[str, str]] = None,
                 description: Optional[str] = None):
        scopes = scopes or {}
        flows = OAuthFlowsModel(authorizationCode={
            'authorizationUrl': authorization_url,
            'tokenUrl': token_url,
            'refreshUrl': refresh_url,
            'scopes': scopes,
        })
        super().__init__(flows=flows, scheme_name=scheme_name, description=description)

    async def __call__(self, request: Request) -> Optional[str]:
        # Get token from headers, or if not specified, from cookies
        authorization: str = request.headers.get('Authorization') or request.cookies.get('Authorization')
        if authorization is not None:
            scheme, param = get_authorization_scheme_param(authorization)
            if scheme.lower() != 'bearer':
                raise HTTPException(
                    status_code=status.HTTP_401_UNAUTHORIZED,
                    detail='Could not validate credentials',
                    headers={'WWW-Authenticate': 'Bearer'},
                )
            return param
        else:
            return None


get_oauth2_token = OAuth2Scheme(authorization_url='auth', token_url='auth/token')


class HTTPUnauthorizedException(HTTPException):
    """ Raising this exception initiates the login process """
    def __init__(self, detail: Optional[Any] = None, headers: Optional[Dict[str, Any]] = None):
        super().__init__(status.HTTP_401_UNAUTHORIZED, detail=detail, headers=headers)


async def get_current_user(token: Optional[str] = Depends(get_oauth2_token)) -> schemas.User:
    """
    Validate and decode token to get current user

    :param token: JWT token
    :return:
    """
    if token is not None:
        try:
            payload = decode_access_token(token)
            return schemas.User(username=payload['sub'])
        except HTTPException as e:
            raise e
        except Exception as e:  # Other exception
            raise HTTPException(
                status_code=status.HTTP_401_UNAUTHORIZED,
                detail='Could not validate credentials',
                headers={'WWW-Authenticate': 'Bearer'},
            ) from e
    else:
        # Redirect to authentication
        raise HTTPUnauthorizedException('Not logged in')


# OAuth2 client ID
CLIENT_ID = 'photosathome'

# TODO: replace with redis or similar
state_to_code_verifier: Dict[bytes, bytes] = {}
state_to_request: Dict[bytes, dict] = {}


async def redirect_authorization(request: Request) -> Response:
    """
    Redirect to authorization server

    :param request: Temporal request object
    :return: Redirect response to auth server
    """
    # Generate random code verifier and compute code challenge
    code_challenge, code_verifier = generate_code_challenge()

    # Generate state and use to lookup code verifier and request info later
    state = base64_random(16)
    state_to_code_verifier[state] = code_verifier
    state_to_request[state] = {
        'method': request.method,
        'url': str(request.url),
        'headers': dict(request.headers),
    }

    redirect_url_params = {
        'response_type': 'code',
        'client_id': CLIENT_ID,
        'redirect_url': f'http://{config.server.hostname}:{config.server.port}/auth/token',
        'code_challenge': code_challenge,
        'code_challenge_method': 'S256',
        'state': state,
    }
    # noinspection PyUnresolvedReferences
    redirect_url = f'http://{config.auth.hostname}:{config.auth.port}/' \
                   f'auth?{urlencode(redirect_url_params)}'

    return RedirectResponse(
        redirect_url,
        status_code=status.HTTP_303_SEE_OTHER,
        headers={'WWW-Authenticate': 'Bearer'}
    )


router = APIRouter(
    prefix='/auth',
    tags=['auth'],
)


@router.get('/')
async def authorize(request: Request) -> Response:
    """ Functionality is declared separately so it can also be used as an exception handler """
    return await redirect_authorization(request)


@router.post('/token')
async def login_for_access_token(authorization_code: str, state: bytes, response: Response
                                 ) -> Union[Response, schemas.Token]:
    """
    Use authorization_code to return an access token from authorization server

    Raises an HTTPException if username and/or password are incorrect

    :param authorization_code: OAuth2 password request form with username/password
    :param state: State to lookup code challenge
    :param response: Temporal response object
    :return: Token
    """

    # TODO: swap tokens

    if (authorization_code is None) or (state is None) or (state not in state_to_code_verifier):
        raise HTTPException(
            status.HTTP_400_BAD_REQUEST,
            detail='Invalid request',
        )

    code_verifier = state_to_code_verifier[state]
    del state_to_code_verifier[state]

    original_request = state_to_request[state]
    del state_to_request[state]

    token_response = requests.post(f'http://{config.auth.hostname}:{config.auth.port}/token', json={
        'grant_type': 'authorization_code',
        'client_id': CLIENT_ID,
        'redirect_url': 'abc',  # TODO
        'code': authorization_code,
        'code_verifier': code_verifier.decode('utf-8'),
    })

    token = schemas.Token(**token_response.json())

    if original_request['method'] == 'GET':
        # Redirect back to original URL
        response.status_code = status.HTTP_303_SEE_OTHER
        response.headers['Location'] = original_request['url']

    response.headers['WWW-Authenticate'] = 'Bearer'
    response.set_cookie(
        'Authorization', f'Bearer {token.access_token}',
        max_age=token.expires_in,
        expires=token.expires_in,
        domain=config.server.hostname,
        httponly=True,
    )

    return token
