import hashlib
import os
import base64

from typing import Tuple

from jose import jwt

from ..config import config

# RSA public key for decoding authorization codes and tokens
with open(config.auth.key.location, 'r') as key_file:
    auth_rsa_public_key = key_file.read()


def base64_random(bytes: int) -> bytes:
    """
    Generate url safe base64-encoded random value

    :param bytes: Length in bytes
    :return: Base64 encoded random value
    """
    return base64.urlsafe_b64encode(os.urandom(bytes)).rstrip(b'=')


def generate_code_challenge() -> Tuple[bytes, bytes]:
    """
    Generate code challenge from random code verifier string using SHA256

    :return: Code challenge, code verifier
    """
    code_verifier = base64.urlsafe_b64encode(os.urandom(32)).rstrip(b'=')
    code_challenge = base64.urlsafe_b64encode(hashlib.sha256(code_verifier).digest()).rstrip(b'=')

    return code_challenge, code_verifier


def decode_access_token(token: str) -> dict:
    """
    Decode access token

    Raises JWTError if unable to validate/decode token

    :param token: JWT token
    :return: Dictionary containing encoded values in payload
    """
    return jwt.decode(token, auth_rsa_public_key, algorithms=['RS256'])
